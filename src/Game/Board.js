import React from 'react';
import FieldItem from './FieldItem';
import generateItems from './BoardHelper';

const board = generateItems(4);

const style = {
  container: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  board: {
    width: 450
  }
}

export default function Board({ size }) {
  return(
    <div style={style.board}>
      <div style={style.container}>
        {board.map(item => <FieldItem id={item.id} key={item.index}/>)} 
      </div>
    </div>
  )
}