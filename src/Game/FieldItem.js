import React from 'react';

function getIconById(id) {
  const icons = [
    "./Game/Assets/cake.svg",
    "./Game/Assets/chili.svg",
    "./Game/Assets/coconut.svg",
    "./Game/Assets/cookie.svg",
    "./Game/Assets/food.svg",
    "./Game/Assets/food1.svg",
    "./Game/Assets/food2.svg",
    "./Game/Assets/hotdog.svg",
    "./Game/Assets/icecream.svg",
    "./Game/Assets/jam.svg",
    "./Game/Assets/lemon.svg",
    "./Game/Assets/pizza.svg",
    "./Game/Assets/raspberry.svg",
    "./Game/Assets/sandwich.svg",
    "./Game/Assets/sandwich1.svg",
    "./Game/Assets/soda.svg",
    "./Game/Assets/soda1.svg",
    "./Game/Assets/sweet.svg",
    "./Game/Assets/sweet1.svg",
    "./Game/Assets/sweet2.svg",
    "./Game/Assets/sweet3.svg",
    "./Game/Assets/toffee.svg",
    "./Game/Assets/turkey.svg",
    "./Game/Assets/watermelon.svg"
  ]
  return +id > icons.length ? null : icons[+id];
}

const style = {
  field: {
    border: '1px black solid',
    borderRadius: 4,
    margin: 2,
    padding: 3
  },
  img: {
    width: 100
  }
}

export default function FieldItem({id}) {
  return (
    <div style={style.field}>
      <img style={style.img} src={getIconById(id)} alt={getIconById(id)}></img>
    </div>
  )
}

