function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

export default function generateItems(size) {
  const len = size * size;
  
  const boardItems = new Array(len);
  
  while(boardItems.filter(x =>x).length !== boardItems.length) {
    const itemId = getRandomInt(10);
    let pos1 = getRandomInt(len-1);
    while(boardItems[pos1] !== undefined) {
      pos1 = pos1 < len-1 ? pos1 + 1 : 0;
    }
    let pos2 = getRandomInt(len-1);
    while (boardItems[pos2] !== undefined || pos1 === pos2) {
      pos2 = pos2 < len-1 ? pos2 + 1 : 0;
    }
    const item = {
      id: itemId,
      uncover: false
    }
    // console.log(pos1,pos2);
    boardItems[pos1] = { ...item, index: pos1 };
    boardItems[pos2] = { ...item, index: pos2 };
  }
  return boardItems;
}
