import React from 'react';
import Board from './Game/Board';

function App() {
  return (
    <div>
      <Board size='4' />
    </div>
  );
}

export default App;
